Transparencia::Application.routes.draw do

  resources :duration_statuses


  resources :monthlies
  resources :travel_expenses
  resources :travel_drivers
  resources :travels
  resources :vehicles
  resources :drivers
  resources :participants
  resources :confirmations
  resources :requests
  resources :addresses
  resources :user_types
  resources :reserve_of_physical_spaces
  resources :reservation_statuses
  resources :alocation_permanents
  resources :spaces
  resources :location_types
  resources :semesters
  resources :recesses
  resources :holidays
  resources :request_users

  root to: 'portal#index'

  # resources :administrators
  get '/administrator/login' => 'administrators#login', as: :administrator_login
  get '/administrator/logout' => 'administrators#logout', as: :administrator_logout
  get '/administrator/welcome' => 'administrators#welcome', as: :administrator_welcome
  post '/administrator/authentication' => 'administrators#authentication', as: :administrator_authentication
  post '/administrator/create' => 'administrators#create', as: :administrator_create
  get '/administrator/new' => 'administrators#new', as: :administrator_new

  #páginas de erro
  get '/notfound' => 'portal#notfound', as: :notfound
  
  resources :users do
    resources :durations
  end
  get '/durations/destroy/user/:id/:duration_id' => 'durations#destroy', as: :destroy_duration 

  resources :roles
  resources :sectors
  resources :institutions
  resources :functions
  resources :locals
  resources :dailies
  resources :durations
  resources :daily_function_locals

  resources :amounts do
    resources :readjustments
  end

  resources :readjustments
  resources :requests

  get 'alocation_permanents/space/:semester_id/espacos' => 'alocation_permanents#espacos', as: 'alocation_permanents_espacos'
  get 'alocation_permanents/space/:id/:semester_id' => 'alocation_permanents#reserves', as: 'alocation_permanents_space'
  get 'alocation_permanents/space/:id/:semester_id/edit' => 'alocation_permanents#editar', as: 'alocation_permanents_editar'
  post 'alocation_permanents/spaces/:space_id/:semester_id/salvar_alocacao' => 'alocation_permanents#salvar_alocacao', as: 'salvar_alocacao'

  get 'reserve_of_physical_spaces/space/all' => 'reserve_of_physical_spaces#espacos', as: 'reserve_of_physical_spaces_espacos'
  get 'reserve_of_physical_spaces/space/:id' => 'reserve_of_physical_spaces#reservas', as: 'reserve_of_physical_spaces_reservas'
  get 'reserve_of_physical_spaces/space/:id/:mes/:ano' => 'reserve_of_physical_spaces#calendario', as: 'calendario'

  get 'reserve/space/:id/:mes/:ano/new' => 'reserve_of_physical_spaces#new', as: 'reserve_of_physical_spaces_reservas_new'
  
  get '/reserve_of_physical_spaces/aprovacoes/list' => 'reserve_of_physical_spaces#aprovacoes', as: 'aprovacoes'
  get '/reserve_of_physical_spaces/approve/:id' => 'reserve_of_physical_spaces#approve', as: 'approve_reserves'
  get '/reserve_of_physical_spaces/non_approve/:id' => 'reserve_of_physical_spaces#non_approve', as: 'non_approve_reserves'
  post '/create/:id/:mes/:ano/reserve_of_physical_space' => 'reserve_of_physical_spaces#create', as: 'create_reserve'
  post '/update/reserve_of_physical_space/:id' => 'reserve_of_physical_spaces#update', as: 'update_reserve'
  #post 'alocation_permanents/spaces/salvar_alocacao' => 'alocation_permanents#salvar_alocacao', as: 'salvar_alocacao'

  #rotas de users
  get '/user/login' => 'users#login', as: 'user_login'
  get '/user/logout' => 'users#logout', as: 'user_logout'
  post '/user/authentication' => 'users#authentication', as: 'user_authentication'
  get '/user/profile/edit' => 'users#edit_profile', as: 'edit_user_profile'
  get '/user/profile/edit/password' => 'users#edit_password', as: 'edit_user_password'
  post '/user/profile/update' => 'users#update_profile', as: 'update_user_profile'
  post '/user/profile/update/password' => 'users#update_password', as: 'update_user_password'

  
  #rotas de confirmations
  get '/confirmations/approve/:id' => 'confirmations#approve', as: 'approve_request'
  post '/confirmations/non_approve/:id' => 'confirmations#non_approve', as: 'non_approve_request'
  get '/confirmations/jus_non_approve/:id' => 'confirmations#non_approve_form', as: 'non_approve_form_request'

  #rota de participants para adicionar users a viagem 
  get '/participants/add/request/:id' => 'participants#index' ,as: 'add_participants'
  post '/participants/add/request/:id' => 'participants#add_participants' ,as: 'add_participants'
   
  #rota para solicitação
  get '/request/user' => 'requests#request_users', as: 'request_users'

  #rotas de confirmações
  get '/confirmations/:id/travel' => 'confirmations#travel', as: 'confirmation_travel'
  get '/confirmations/request/:id/travel/new' => 'travels#new', as: 'confirmation_new_travel'
  get '/confirmations/request/:id/travel/edit' => 'travels#edit', as: 'confirmation_edit_travel'
  get '/confirmations/:id/travel/show' => 'confirmations#show_travel', as: 'confirmation_show_travel'



  #rotas para adicionar motoristas a viagem
  get '/travels/:id/travel_drivers' => 'travel_drivers#index', as: 'add_driver'
  post '/travels/:id/travel_drivers' => 'travel_drivers#add_drivers', as: 'add_driver'

  get '/relatorios/lista-de-veiculos' => 'reports#imprimir_veiculos', as: 'imprimir_veiculos'
  
  # get '/home/administrator' => 'portal#home_administrator', as: 'home_administrator'
  # get '/home/diretor' => 'portal#home_diretor', as: 'home_diretor'
  # get '/home/caf' => 'portal#home_caf', as: 'home_caf'
  # get '/home/patrimonio' => 'portal#home_patrimonio', as: 'home_patrimonio'
  # get '/home/transporte' => 'portal#home_transporte', as: 'home_transporte'
  # get '/home/coordenador' => 'portal#home_nivel_2', as: 'home_nivel_2'
  # get '/home/funcionario_publico' => 'portal#home_nivel_1', as: 'home_nivel_1'
  # get '/home/aluno' => 'portal#home_aluno', as: 'home_aluno'
  # get '/home/externo' => 'portal#home_externo', as: 'home_externo'

  get '/periods' => 'periods#index', as: 'periods'
  post '/periods' => 'periods#create', as: 'periods'
  get '/periods/new' => 'periods#new', as: 'new_period'
  get '/periods/:id/edit' => 'periods#edit', as: 'edit_period'
  get '/periods/:id' => 'periods#show', as: 'period'
  put '/periods/:id' => 'periods#update', as: 'period'
  delete '/periods/:id' => 'periods#destroy', as: 'period'
  

  get '/blocks' => 'blocks#index', as: 'blocks'
  post '/blocks' => 'blocks#create', as: 'blocks'
  get '/blocks/new' => 'blocks#new', as: 'new_block'
  get '/blocks/:id/edit' => 'blocks#edit', as: 'edit_block'
  #get '/blocks/:id' => 'blocks#show', as: 'block'
  put '/blocks/:id' => 'blocks#update', as: 'block'
  delete '/blocks/:id' => 'blocks#destroy', as: 'block'

  
  get '/local_types' => 'local_types#index', as: 'local_types'
  post '/local_types' => 'local_types#create', as: 'local_types'
  get '/local_types/new' => 'local_types#new', as: 'new_local_type'
  get '/local_types/:id/edit' => 'local_types#edit', as: 'edit_local_type'
  #get '/local_types/:id' => 'local_types#show', as: 'local_type'
  put '/local_types/:id' => 'local_types#update', as: 'local_type'
  delete '/local_types/:id' => 'local_types#destroy', as: 'local_type'


  get '/reserves_type' => 'reserves_type#index', as: 'reserves_type'
  post '/reserves_type' => 'reserves_type#create', as: 'reserves_type'
  get '/reserves_type/new' => 'reserves_type#new', as: 'new_reserve_type'
  get '/reserves_type/:id/edit' => 'reserves_type#edit', as: 'edit_reserve_type'
  get '/reserves_type/:id' => 'reserves_type#show', as: 'reserve_type'
  put '/reserves_type/:id' => 'reserves_type#update', as: 'reserve_type'
  delete '/reserves_type/:id' => 'reserves_type#destroy', as: 'reserve_type'



  get '/places' => 'places#index', as: 'places'
  post '/places' => 'places#create', as: 'places'
  get '/places/new' => 'places#new', as: 'new_place'
  get '/places/:id/edit' => 'places#edit', as: 'edit_place'
  get '/places/:id' => 'places#show', as: 'place'
  put '/places/:id' => 'places#update', as: 'place'
  delete '/places/:id' => 'places#destroy', as: 'place'

end