class CreateReserves < ActiveRecord::Migration
  def change
    create_table :reserves do |t|
      t.string :event
      t.boolean :free
      t.boolean :payment_confirmed
      t.text :justification
      t.float :price
      t.integer :approver
      t.references :user
      t.references :reservation_status
      t.references :reserve_type
      t.boolean :deleted

      t.timestamps
    end
    add_index :reserves, :user_id
    add_index :reserves, :reservation_status_id
    add_index :reserves, :reserve_type_id
  end
end
