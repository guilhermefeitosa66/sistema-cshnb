class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :campus
      t.boolean :deleted

      t.timestamps
    end
  end
end
