class CreateReservesScheduleSpace < ActiveRecord::Migration
  def change
    create_table :reserves_schedule_space do |t|
      t.references :period
      t.references :place
      t.references :reserve
      t.integer :schedule
      t.boolean :deleted

      t.timestamps
    end
    add_index :reserves_schedule_space, :period_id
    add_index :reserves_schedule_space, :place_id
    add_index :reserves_schedule_space, :reserve_id
  end
end
