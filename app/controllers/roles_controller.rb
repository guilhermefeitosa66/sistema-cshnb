class RolesController < ApplicationController

  include AdministratorsHelper
  include UsersHelper

  before_filter except: [] do
    redirect_to notfound_path unless ((user_authenticated? && upper_lvl_3?) || administrator_authenticated?)
  end
  
  def index
    @roles = Role.where(deleted: false)
  end

  def show
    @role = Role.find(params[:id])
  end

  def new
    @role = Role.new
  end

  def edit
    @role = Role.find(params[:id])
  end

  def create
    @role = Role.new(params[:role])
    @role.deleted = false
    
    if @role.save
      redirect_to roles_path
    else
      render :new
    end
  end

  def update
    @role = Role.find(params[:id])

    if @role.update_attributes(params[:role])
      redirect_to roles_path
    else
      render :edit
    end
  end

  def destroy
    @role = Role.find(params[:id])
    @role.update_attribute(:deleted, true)
    redirect_to roles_path
  end
end