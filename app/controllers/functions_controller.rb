#encoding: utf-8
class FunctionsController < ApplicationController
  include UsersHelper
  include AdministratorsHelper

  before_filter except: [] do
    if !(administrator_authenticated? || (user_authenticated? && upper_lvl_3?))
      redirect_to notfound_path
    end
  end
  
  def index
    @functions = Function.where(deleted: false)
  end

  def show
    @function = Function.find(params[:id])
  end

  def new
    @function = Function.new
  end

  def edit
    @function = Function.find(params[:id])
  end

  def create
    @function = Function.new(params[:function])
    @function.deleted = false
    if @function.level == 3
      fun = Function.where( deleted: false, level: 3)
    elsif @function.level == 4
      fun = Function.where( deleted: false, level: 4)
    else
      fun=[]
    end
    if fun.empty?
      if @function.save
        redirect_to functions_path
      else
       render :new 
      end
    else
    flash[:warning] = 'Já existe uma função para esse nível'  
    render :new
    end   
  end

  def update
    @function = Function.find(params[:id])
    if @function.level == 3
      fun = Function.where( deleted: false, level: 4)
    elsif @function.level == 4
      fun = Function.where( deleted: false, level: 3)
    else
      fun=[]
    end
    function = Function.new(params[:function])
    if fun.empty? || (@function.level == function.level)
      if @function.update_attributes(params[:function])
        redirect_to functions_path
      else
        render :edit
      end
    else
    flash[:warning] = 'Já existe uma função para esse nível'  
    render :edit
    end
  end

  def destroy
    @function = Function.find(params[:id])
    @function.update_attribute(:deleted, true)
    redirect_to functions_path
  end
end
