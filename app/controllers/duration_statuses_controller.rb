class DurationStatusesController < ApplicationController
  
  def index
    @duration_statuses = DurationStatus.where(deleted: false)
  end

  def show
    @duration_status = DurationStatus.find(params[:id])
  end

  def new
    @duration_status = DurationStatus.new
  end

  def edit
    @duration_status = DurationStatus.find(params[:id])
  end

  def create
    @duration_status = DurationStatus.new(params[:duration_status])
    @duration_status.deleted = false

      if @duration_status.save
        redirect_to duration_statuses_path
      else
        render :new
      end
  end

  def update
    @duration_status = DurationStatus.find(params[:id])

      if @duration_status.update_attributes(params[:duration_status])
        redirect_to duration_statuses_path
      else
        render :edit 
      end
  end

  def destroy
    @duration_status = DurationStatus.find(params[:id])
    @duration_status.update_attribute(:deleted, true)
    redirect_to duration_statuses_path
  end
end
