class AdministratorsController < ApplicationController

  # before_filter :permission_admin_not_logged, only: [:login, :new, :create, :recovery_password, :authentication]
  # before_filter :permission_admin_logged, only: [:welcome, :logout, :update]  
  include AdministratorsHelper
  
  before_filter only: [:welcome] do
    redirect_to notfound_path unless administrator_authenticated?
  end

  before_filter only: [:new, :create] do
    if Administrator.count > 0
      redirect_to notfound_path
    end
  end
  
  before_filter only: [:login] do
    if Administrator.count == 0
      redirect_to administrator_new_path      
    elsif administrator_authenticated?
      redirect_to administrator_welcome_path
    end
  end

  before_filter only: [:authentication] do
   redirect_to administrator_welcome_path if administrator_authenticated?  
  end 

  def login
    @administrator = Administrator.new
  end

  def logout
    end_session
    redirect_to administrator_login_path
  end

  def welcome
  end

  def authentication
    @administrator = Administrator.new(params[:administrator])
    if authenticate_administrator(@administrator)
      redirect_to root_path
    else
      flash[:error] = :authentication_error
      render :login
    end
  end

  def new
    @administrator = Administrator.new
  end

  def create
    @administrator = Administrator.new(params[:administrator])
    if @administrator.save
      flash[:success] = t 'messages.save_success'
      redirect_to administrator_login_path
    else
      render :new
    end
  end
end
