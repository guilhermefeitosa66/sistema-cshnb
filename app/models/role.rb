class Role < ActiveRecord::Base
  has_many :users, dependent: :destroy
  
  attr_accessible :deleted,
                  :name,
                  :users_attributes

  validates :name, uniqueness:{case_sensitive: true} 
end
