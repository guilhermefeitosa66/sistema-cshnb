class ReserveType < ActiveRecord::Base
  has_many :reserves, :dependent => :destroy
  attr_accessible :deleted, :name, :reserves_attributes
end
