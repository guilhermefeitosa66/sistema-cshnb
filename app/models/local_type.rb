class LocalType < ActiveRecord::Base
  has_many :places, :dependent => :destroy
  attr_accessible :deleted, :name, :price_local, :places_attributes
end
