class DurationStatus < ActiveRecord::Base
  has_many :durations , :dependent => :destroy
  attr_accessible :deleted, :role, :durations_attributes
end
