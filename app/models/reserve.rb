class Reserve < ActiveRecord::Base
  belongs_to :user
  belongs_to :reservation_status
  belongs_to :reserve_type
  has_many :reserves_schedule_space, :dependent => :destroy
  attr_accessible :approver, :deleted, :event, :free, :justification, :payment_confirmed, :price, :reserves_schedule_space_attributes, :user_id, :reservation_status_id, :reserve_type_id
end
