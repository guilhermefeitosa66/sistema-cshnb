class Local < ActiveRecord::Base
  has_many :daily_function_locals
  
  attr_accessible :deleted,
                  :local,
                  :daily_function_locals_attributes

  validates :local, presence: true
  validates :local, uniqueness: {case_sensitive: true} 
  
end
