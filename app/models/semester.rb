#encoding: utf-8
class Semester < ActiveRecord::Base
  has_many :alocation_permanents, :dependent => :destroy

  attr_accessible :deleted,
                  :end_date,
                  :name,
                  :start_date,
                  :alocation_permanents_attributes

  validates :name, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  validates :data_do_fim, acceptance: {accept: true, message: ' menor que data do início ou campo está em branco'}
  validates :data_de_inicio_deste_periodo, acceptance: {accept: true, message: ' menor que data do fim do periodo anterior ou campo está em branco'}

  def data_do_fim
    return false if self.start_date.nil? || self.end_date.nil?
    if self.end_date < self.start_date
  	  return false
    else
   	  return true
    end
  end

  def data_de_inicio_deste_periodo
    semester = Semester.where(deleted: false).last
    return false if self.start_date.nil?

    if semester.nil?
      return true
    else
      puts self.start_date
      puts semester.end_date
      if self.start_date <= semester.end_date
        return false
      else
        return true
      end  
    end
  end
end
