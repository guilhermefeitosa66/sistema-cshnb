class Place < ActiveRecord::Base
  belongs_to :local_type
  belongs_to :block
  has_many :reserves_schedule_space, :dependent => :destroy
  attr_accessible :capacity, :deleted, :name, :observations, :size, :reserves_schedule_space_attributes, :local_type_id, :block_id
end
