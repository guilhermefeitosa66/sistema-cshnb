class Block < ActiveRecord::Base
  has_many :places, :dependent => :destroy
  attr_accessible :deleted, :name, :places_attributes
end
