class ReservationStatus < ActiveRecord::Base
  has_many :reserve_of_physical_spaces, :dependent => :destroy
  has_many :reserves, :dependent => :destroy


  attr_accessible :deleted,
                  :name,
                  :reserve_of_physical_spaces_attributes,
                  :reserves_attributes

  validates :name, presence: true
end
