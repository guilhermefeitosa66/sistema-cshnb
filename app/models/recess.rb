class Recess < ActiveRecord::Base
  attr_accessible :deleted,
                  :begin,
                  :description,
                  :end

  validates :begin, presence: true
  validates :end, presence: true
  validates :description, presence: true
  validates :data_final, acceptance: {accept: true, message: ' menor que data inicial'}
  
  def data_final
    if self.end < self.begin
  	  return false
  	end
   	return true
  end
end
