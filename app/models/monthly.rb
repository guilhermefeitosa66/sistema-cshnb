class Monthly < ActiveRecord::Base
  belongs_to :driver
  belongs_to :travel

  attr_accessible :data,
  				        :qtde,
  				        :transfer,
  				        :deleted,
                  :travel_id,
                  :driver_id
end
