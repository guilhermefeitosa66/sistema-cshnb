class Period < ActiveRecord::Base
  has_many :reserves_schedule_space, :dependent => :destroy
  attr_accessible :deleted, :end_date, :name, :start_date, :reserves_schedule_space_attributes
end
