#encoding: utf-8
class Duration < ActiveRecord::Base
  belongs_to :user
  belongs_to :function
  belongs_to :sector
  belongs_to :duration_status
  
  attr_accessible :deleted,
                  :acting,
                  :end,
                  :initiation,
                  :act_number,
                  :user_id,
                  :function_id,
                  :sector_id,
                  :duration_status_id

  validates :initiation, presence: true
  validates :function_id, presence: true
  validates :sector_id, presence: true
  validates :act_number, presence: true
  validates :duration_status_id, presence: true
  validates :is_public_employee, acceptance: {accept: true, message: ', Somente para Funcionário Público'}
  validates :no_two_bosses, acceptance: {accept: true, message: ', Somente 1 chefe de setor permanente (dentro ou fora de exercício) e 1 interino neste Departamento/Curso'}
  validates :no_two_directors, acceptance: {accept: true, message: ', Condição inválida para vigência de diretor'}
  validates :no_two_cafs, acceptance: {accept: true, message: ', Condição inválida para vigência de Coordenador Administrativo Financeiro'}

  after_save do |duration|
    if duration.duration_status_id == 2
      setor = Sector.find(self.sector_id)
      durations = Duration.where(sector_id: setor.id, end: nil, deleted: false)
      durations = durations.select {|duration| duration.function.level >= 2 && duration.duration_status_id == 1}
      if !durations.empty?
        durations.first.update_attribute(:duration_status_id, 3)
      end
    elsif duration.duration_status_id == 1 
      setor = Sector.find(self.sector_id)
      durations = Duration.where(sector_id: setor.id, end: nil, deleted: false)
      durations = durations.select {|duration| duration.function.level >= 2 && duration.duration_status_id == 2}
      if !durations.empty?
        durations.first.update_attribute(:end, Time.now)
      end
    end
  end
  
  def is_public_employee
    if self.user.role_id == nil || self.user.siape == nil || self.user.user_type_id > 1
      return false 
    else
      return true
    end
  end
  # def director
  #   if self.function.level == 4
  #     duration = Duration.where(end: nil, deleted: false)
  #     duration = duration.select { |d| d.function.level == 4}

  #   end
  # end

  def no_boss_permanent(query)
   # setor = Sector.find(self.sector_id)
    durations = query
    durations = durations.select {|duration| duration.function.level >= 2 && (duration.duration_status_id == 1 || duration.duration_status_id == 3)}
    durations = durations.reject {|duration| duration.id == self.id}

    if durations.empty?
      return true
    else
      return false
    end
  end

  def no_boss_temporary(query)
    #setor = Sector.find(self.sector_id)
    
    durations = query
    durations = durations.select {|duration| duration.function.level >= 2 && duration.duration_status_id == 2}
    durations = durations.reject {|duration| duration.id == self.id}
    if durations.empty?
      return true
    else
      return false        
    end
  end

  def no_two_bosses
    if self.function.level >= 2
      setor = Sector.find(self.sector_id)
      durations = Duration.where(sector_id: setor.id, end: nil, deleted: false)
      x = no_boss_temporary(durations)
      y = no_boss_permanent(durations)
      #if (one_boss_permanent(durations) && self.duration_status_id == 2) && one_boss_temporary(durations)
      if x && self.duration_status_id == 2
        return true
      elsif y && self.duration_status_id != 2
        return true
      elsif !y && self.duration_status_id == 2
        return true
      else
        return false
      end
    else
      return true
    end
  end

  def no_two_directors
    if self.function.level == 4
       durations = Duration.where(end: nil, deleted: false)
       durations = durations.select { |d| d.function.level == 4}
      x = no_boss_temporary(durations)
      y = no_boss_permanent(durations)
      #if (one_boss_permanent(durations) && self.duration_status_id == 2) && one_boss_temporary(durations)
      if x && self.duration_status_id == 2
        return true
      elsif y && self.duration_status_id != 2
        return true
      elsif !y && self.duration_status_id == 2
        return true
      else
        return false
      end
    else
      return true
    end
  end
  
  def no_two_cafs
    if self.function.level == 3
       durations = Duration.where(end: nil, deleted: false)
       durations = durations.select { |d| d.function.level == 3}
      x = no_boss_temporary(durations)
      y = no_boss_permanent(durations)
      #if (one_boss_permanent(durations) && self.duration_status_id == 2) && one_boss_temporary(durations)
      if x && self.duration_status_id == 2
        return true
      elsif y && self.duration_status_id != 2
        return true
      elsif !y && self.duration_status_id == 2
        return true
      else
        return false
      end
    else
      return true
    end
  end




  # def no_two_bosses
  #   setor = Sector.find(self.sector_id)
    
  #   durations = Duration.where(sector_id: setor.id, end: nil, deleted: false)
  #   durations = durations.select {|duration| duration.function.level >= 2}

  #   if durations.empty?
  #     return true
  #   elsif durations.count == 2
  #     return false
  #   elsif ((durations.first.duration_status_id == 1 || durations.first.duration_status_id == 3) && self.duration_status_id == 2)
  #     return true
  #   elsif (durations.first.duration_status_id == 2 && self.duration_status_id == 1)
  #     return true
  #   elsif (self.function.level == 1 && self.duration_status_id ==1)
  #     return true
  #   else
  #     return false        
  #   end

    # if durations.empty?
    #   return true
    # elsif durations.count == 2
    #   return false
    # elsif ((durations.first.duration_status_id == 1 || durations.first.duration_status_id == 3) && self.duration_status_id == 2)
    #   return true
    # elsif (durations.first.duration_status_id == 2 && self.duration_status_id == 1)
    #   return true
    # else
    #   return false        
    # end

    # setor.durations.each do |user|
    #   if !(user.durations.last.nil? && user.deleted == false && user.duration.last.duration_status_id < 3)
    #     boss = user if (user.durations.last.function.level >= 2 && user.durations.last.deleted == false)
    #   end
    # end
  # end  
end