require 'test_helper'

class ReserveOfPhysicalSpacesControllerTest < ActionController::TestCase
  setup do
    @reserve_of_physical_space = reserve_of_physical_spaces(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reserve_of_physical_spaces)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reserve_of_physical_space" do
    assert_difference('ReserveOfPhysicalSpace.count') do
      post :create, reserve_of_physical_space: { authorized: @reserve_of_physical_space.authorized, date: @reserve_of_physical_space.date, end_time: @reserve_of_physical_space.end_time, event: @reserve_of_physical_space.event, free: @reserve_of_physical_space.free, justification: @reserve_of_physical_space.justification, payment_confirmed: @reserve_of_physical_space.payment_confirmed, price: @reserve_of_physical_space.price, start_time: @reserve_of_physical_space.start_time }
    end

    assert_redirected_to reserve_of_physical_space_path(assigns(:reserve_of_physical_space))
  end

  test "should show reserve_of_physical_space" do
    get :show, id: @reserve_of_physical_space
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reserve_of_physical_space
    assert_response :success
  end

  test "should update reserve_of_physical_space" do
    put :update, id: @reserve_of_physical_space, reserve_of_physical_space: { authorized: @reserve_of_physical_space.authorized, date: @reserve_of_physical_space.date, end_time: @reserve_of_physical_space.end_time, event: @reserve_of_physical_space.event, free: @reserve_of_physical_space.free, justification: @reserve_of_physical_space.justification, payment_confirmed: @reserve_of_physical_space.payment_confirmed, price: @reserve_of_physical_space.price, start_time: @reserve_of_physical_space.start_time }
    assert_redirected_to reserve_of_physical_space_path(assigns(:reserve_of_physical_space))
  end

  test "should destroy reserve_of_physical_space" do
    assert_difference('ReserveOfPhysicalSpace.count', -1) do
      delete :destroy, id: @reserve_of_physical_space
    end

    assert_redirected_to reserve_of_physical_spaces_path
  end
end
