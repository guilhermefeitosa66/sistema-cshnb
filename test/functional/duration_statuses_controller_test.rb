require 'test_helper'

class DurationStatusesControllerTest < ActionController::TestCase
  setup do
    @duration_status = duration_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:duration_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create duration_status" do
    assert_difference('DurationStatus.count') do
      post :create, duration_status: { deleted: @duration_status.deleted, role: @duration_status.role }
    end

    assert_redirected_to duration_status_path(assigns(:duration_status))
  end

  test "should show duration_status" do
    get :show, id: @duration_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @duration_status
    assert_response :success
  end

  test "should update duration_status" do
    put :update, id: @duration_status, duration_status: { deleted: @duration_status.deleted, role: @duration_status.role }
    assert_redirected_to duration_status_path(assigns(:duration_status))
  end

  test "should destroy duration_status" do
    assert_difference('DurationStatus.count', -1) do
      delete :destroy, id: @duration_status
    end

    assert_redirected_to duration_statuses_path
  end
end
